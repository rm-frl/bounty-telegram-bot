﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BountyTelegramBot.Models
{
    public class TelegramUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int Id { get; set; }

        public long ChatId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CurrentDialogClassName { get; set; }

        public string CurrentStateClassName { get; set; }

        public virtual TelegramUserInfo Info { get; set; }

        public virtual List<ReferalUser> Referals { get; set; }
    }
}
