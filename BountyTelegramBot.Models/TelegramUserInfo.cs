﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BountyTelegramBot.Models
{
    public class TelegramUserInfo
    {
        [Key]
        public int Id { get; set; }

        public int TelegramUserId { get; set; }

        public string Email { get; set; }

        public string TwitterProfileUrl { get; set; }

        public string InstagramProfile { get; set; }

        public string FacebookProfile { get; set; }

        public string EthereumAddress { get; set; }

        [ForeignKey("TelegramUserId")]
        public TelegramUser User { get; set; }
    }
}
