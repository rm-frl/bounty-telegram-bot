﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BountyTelegramBot.Models;
using Microsoft.EntityFrameworkCore;

namespace BountyTelegramBot.DAL
{
    public class UserInfoRepository : IUserInfoRepository
    {
        DatabaseContext context;

        public UserInfoRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public void CreateInfo(TelegramUserInfo info)
        {
            context.UsersInfo.Add(info);
            context.SaveChanges();
        }

        public TelegramUserInfo GetInfo(int telegramUserId)
        {
            var userInfo = context.UsersInfo.FirstOrDefault(x=>x.TelegramUserId == telegramUserId);
            return userInfo;
        }

        public async Task<TelegramUserInfo> GetInfoAsync(int telegramUserId)
        {
            var userInfo = await context.UsersInfo.FirstOrDefaultAsync(x => x.TelegramUserId == telegramUserId);
            return userInfo;
        }

        public void RemoveInfo(int telegramUserId)
        {
            var userInfo = context.Users.Find(telegramUserId).Info;
            context.UsersInfo.Remove(userInfo);
            context.SaveChanges();
        }

        public void UpdateInfo(TelegramUserInfo info)
        {
            context.UsersInfo.Update(info);
            context.SaveChanges();
        }

        public async Task UpdateInfoAsync(TelegramUserInfo info)
        {
            context.UsersInfo.Update(info);
            await context.SaveChangesAsync();
        }
    }
}
