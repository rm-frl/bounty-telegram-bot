﻿using BountyTelegramBot.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BountyTelegramBot.DAL
{
    public interface IUserInfoRepository
    {
        Task<TelegramUserInfo> GetInfoAsync(int telegramUserId);

        TelegramUserInfo GetInfo(int telegramUserId);

        void CreateInfo(TelegramUserInfo info);

        void UpdateInfo(TelegramUserInfo info);

        Task UpdateInfoAsync(TelegramUserInfo info);

        void RemoveInfo(int telegramUserId);

    }
}
