﻿using BountyTelegramBot.DAL;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class RemoveReferalCommandHandler : AsyncRequestHandler<RemoveReferalCommand, bool>
    {
        IReferalUserRepository refUserRepository;

        public RemoveReferalCommandHandler(IReferalUserRepository userRepository)
        {
            this.refUserRepository = userRepository;
        }

        protected override async Task<bool> HandleCore(RemoveReferalCommand request)
        {
            return refUserRepository.RemoveReferal(request.ReferalId);
        }
    }
}
