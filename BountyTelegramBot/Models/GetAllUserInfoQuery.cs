﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class GetAllUserInfoQuery: IRequest<TelegramUserInfo>
    {
        public int TelegramUserId { get; private set; }

        public GetAllUserInfoQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }
}
