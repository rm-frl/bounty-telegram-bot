﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class SaveReferalsCommand: IRequest<bool>
    {
        public int FromId { get; set; }
        public List<ReferalUser> Referals { get; set; }

        public SaveReferalsCommand(int fromId, List<ReferalUser> referals)
        {
            FromId = fromId;
            Referals = referals;
        }
    }
}
