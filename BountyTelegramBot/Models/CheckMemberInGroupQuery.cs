﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class CheckMemberInGroupQuery: IRequest<string>
    {
        public int TelegramUserId { get; private set; }

        public CheckMemberInGroupQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }
}
