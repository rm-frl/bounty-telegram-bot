﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class CheckFollowersInTwitterQuery: IRequest<bool>
    {
        public string UserName { get; set; }

        public CheckFollowersInTwitterQuery(string userName)
        {
            UserName = userName;
        }
    }
}
