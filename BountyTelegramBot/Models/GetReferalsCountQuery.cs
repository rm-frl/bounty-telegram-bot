﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class GetReferalsCountQuery: IRequest<int>
    {
        public int TelegramUserId { get; set; }

        public GetReferalsCountQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }
}
