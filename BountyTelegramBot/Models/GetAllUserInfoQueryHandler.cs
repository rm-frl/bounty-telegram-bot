﻿using BountyTelegramBot.DAL;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class GetAllUserInfoQueryHandler : AsyncRequestHandler<GetAllUserInfoQuery, TelegramUserInfo>
    {
        IUserInfoRepository userInfoRepository;

        public GetAllUserInfoQueryHandler(IUserInfoRepository userInfoRepository)
        {
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task<TelegramUserInfo> HandleCore(GetAllUserInfoQuery request)
        {
            var userInfo = await userInfoRepository.GetInfoAsync(request.TelegramUserId);
            return userInfo;
        }
    }
}
