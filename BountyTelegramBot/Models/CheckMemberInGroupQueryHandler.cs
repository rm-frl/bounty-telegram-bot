﻿using BountyTelegramBot.Interfaces;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;

namespace BountyTelegramBot.Models
{
    public class CheckMemberInGroupQueryHandler : AsyncRequestHandler<CheckMemberInGroupQuery, string>
    {
        IClientService telegramService;
        string targetGroupName;

        public CheckMemberInGroupQueryHandler(IOptions<TelegramConfiguration> config, IClientService telegramService)
        {
            this.telegramService = telegramService;
            this.targetGroupName = config.Value.TargetGroupName;
        }

        protected override async Task<string> HandleCore(CheckMemberInGroupQuery request)
        {
            var member = telegramService.Client.GetChatMemberAsync(targetGroupName, request.TelegramUserId).Result;
            if (member != null &&
                member.Status == ChatMemberStatus.Member ||
                member.Status == ChatMemberStatus.Creator ||
                member.Status == ChatMemberStatus.Administrator)
                return targetGroupName;
            else
                return null;
        }
    }
}
