﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class RemoveReferalCommand : IRequest<bool>
    {
        public int ReferalId { get; set; }

        public RemoveReferalCommand(int referalId)
        {
            ReferalId = referalId;
        }
    }
}
