﻿using BountyTelegramBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class CheckFollowersInTwitterQueryHandler : AsyncRequestHandler<CheckFollowersInTwitterQuery, bool>
    {
        ITwitterService twitterService;

        public CheckFollowersInTwitterQueryHandler(ITwitterService twitterService)
        {
            this.twitterService = twitterService;
        }

        protected override async Task<bool> HandleCore(CheckFollowersInTwitterQuery request)
        {
            return twitterService.IsFollowUser(request.UserName, twitterService.TargetGroupName);
        }
    }
}
