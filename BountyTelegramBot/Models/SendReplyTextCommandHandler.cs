﻿using BountyTelegramBot.DAL;
using BountyTelegramBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class SendReplyTextCommandHandler : AsyncRequestHandler<SendReplyTextCommand, bool>
    {
        IClientService telegramService;

        public SendReplyTextCommandHandler(IClientService telegramService)
        {
            this.telegramService = telegramService;
        }

        protected override async Task<bool> HandleCore(SendReplyTextCommand request)
        {
            string message = "null";
            if (request.Reply.Update != null && request.Reply.Update.Message != null)
                message = request.Reply.Update.Message.Text;
            try
            {
                await telegramService.Client.SendTextMessageAsync(
                        chatId: request.UserId,
                        text: message,
                        parseMode: request.Reply.ParseMode,
                        disableWebPagePreview: request.Reply.DisableWebPageView,
                        replyMarkup: request.Reply.ReplyMarkup
                        );
            }catch(Exception e)
            {

            }
            return true;
        }
    }
}
