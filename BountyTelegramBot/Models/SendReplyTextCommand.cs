﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace BountyTelegramBot.Models
{
    public class SendReplyTextCommand: IRequest<bool>
    {
        public int UserId { get; private set; }

        public ReplyTextMessage Reply { get; private set; }

        public SendReplyTextCommand(int userId, ReplyTextMessage reply)
        {
            UserId = userId;
            Reply = reply;
        }
    }
}
