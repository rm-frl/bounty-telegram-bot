﻿using BountyTelegramBot.Dialogs;
using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class DialogFactory
    {
        public static readonly string START_COMMAND = "/start";
        public static readonly string RESET_COMMAND = "/reset";

        #region UserInfo props name
        public static readonly string EMAIL_FIELD = "Email";
        public static readonly string TWITTER_FIELD = "TwitterProfileUrl";
        public static readonly string INSTAGRAM_FIELD = "InstagramProfile";
        public static readonly string FACEBOOK_FIELD = "FacebookProfile";
        public static readonly string ETHEREUM_FIELD = "EthereumAddress";
        #endregion

        #region 0. Main Menu Dialogs
        public static BaseDialog CreateMainMenuDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new MainMenuDialog(mediator, currentState, telegramUserId);
            return dialog;
        }
        #endregion

        #region 1. Create Profile Dialogs
        public static BaseDialog CreateProfileDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new CreateProfileDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateEmailDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new EmailDialog(mediator, currentState, telegramUserId, EMAIL_FIELD);
            return dialog;
        }

        public static BaseDialog CreateTwitterDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new TwitterDialog(mediator, currentState, telegramUserId, TWITTER_FIELD);
            return dialog;
        }

        public static BaseDialog CreateInstagramDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new InstagramDialog(mediator, currentState, telegramUserId, INSTAGRAM_FIELD);
            return dialog;
        }

        public static BaseDialog CreateFacebookDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new FacebookDialog(mediator, currentState, telegramUserId, FACEBOOK_FIELD);
            return dialog;
        }

        public static BaseDialog CreateEthereumDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new EthereumDialog(mediator, currentState, telegramUserId, ETHEREUM_FIELD);
            return dialog;
        }
        #endregion

        #region 2. Show Details Dialogs
        public static BaseDialog CreateShowDetailsDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new ShowDetailsDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateMyProfileDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new MyProfileDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateGroupInvitersDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new GroupInvitersDialog(mediator, currentState, telegramUserId);
            return dialog;
        }
        #endregion
    }
}
