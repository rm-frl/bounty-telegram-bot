﻿using BountyTelegramBot.DAL;
using BountyTelegramBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class UpdateReferalCountInGoogleDriveCommandHandler : AsyncRequestHandler<UpdateReferalCountInGoogleDriveCommand, bool>
    {
        IReferalUserRepository referalUserRepository;
        IGoogleDriveService googleDriveService;

        public UpdateReferalCountInGoogleDriveCommandHandler(
            IReferalUserRepository referalUserRepository, IGoogleDriveService googleDriveService)
        {
            this.referalUserRepository = referalUserRepository;
            this.googleDriveService = googleDriveService;
        }

        protected override async Task<bool> HandleCore(UpdateReferalCountInGoogleDriveCommand request)
        {
                await googleDriveService.ExportUserInfo(request.FromId);
                return true;
        }
    }
}
