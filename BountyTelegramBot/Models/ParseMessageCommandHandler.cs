﻿using BountyTelegramBot.DAL;
using BountyTelegramBot.Interfaces;
using BountyTelegramBot.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;

namespace BountyTelegramBot.Models
{
    public class ParseMessageCommandHandler: AsyncRequestHandler<ParseMessageCommand, bool>
    {
        IMediator mediator;
        IDialogService dialogService;
        IUserRepository userRepository;
        IReferalUserRepository refRepository;

        public ParseMessageCommandHandler(IMediator mediator, IUserRepository userRepository, IDialogService dialogService, IReferalUserRepository refRepository)
        {
            this.mediator = mediator;
            this.dialogService = dialogService;
            this.userRepository = userRepository;
            this.refRepository = refRepository;
        }

        protected override async Task<bool> HandleCore(ParseMessageCommand request)
        {
            if (request.Update.Message.Type != MessageType.ServiceMessage &&
                request.Update.Message.Chat.Type == ChatType.Private)
            {
                var user = userRepository.GetUser(request.Update.Message.From.Id);
                if (user == null)
                {
                    user = new TelegramUser()
                    {
                        Id = request.Update.Message.From.Id,
                        ChatId = request.Update.Message.Chat.Id,
                        FirstName = request.Update.Message.From.FirstName,
                        LastName = request.Update.Message.From.LastName,
                        UserName = request.Update.Message.From.Username,
                    };
                    var createUserNotification = new CreateUserNotification(user);
                    mediator.Publish(createUserNotification);
                }

                if (request.Update.Message.Text == "/start")
                {
                    user.CurrentDialogClassName = typeof(Dialogs.MainMenuDialog).AssemblyQualifiedName;
                    user.CurrentStateClassName = null;
                }

                return dialogService.SendReplyToUser(request.Update, user);
            }
            else if(
                request.Update.Message.Type == MessageType.ServiceMessage &&
                request.Update.Message.From != null &&
                request.Update.Message.NewChatMember != null &&
                request.Update.Message.NewChatMembers != null &&
                request.Update.Message.NewChatMembers.Length > 0 &&
                request.Update.Message.From.Id != request.Update.Message.NewChatMember.Id &&
                !request.Update.Message.NewChatMember.IsBot)
            {
                List<ReferalUser> referals = new List<ReferalUser>();

                foreach (var r in request.Update.Message.NewChatMembers)
                {
                    referals.Add(new ReferalUser()
                    {
                        ReferalUserId = r.Id,
                        UserName = r.Username,
                        FirstName = r.FirstName,
                        LastName = r.LastName
                    });
                }
                SaveReferalsCommand saveReferalsCommand = new SaveReferalsCommand(request.Update.Message.From.Id, referals);
                mediator.Send(saveReferalsCommand).Wait();

                foreach (var r in referals)
                {
                    UpdateReferalCountInGoogleDriveCommand updateReferalsCommand = new UpdateReferalCountInGoogleDriveCommand(r.ReferalUserId);
                    mediator.Send(updateReferalsCommand).Wait();
                }

                return true;
            }
            else if (request.Update.Message.Type == MessageType.ServiceMessage &&
                 request.Update.Message.LeftChatMember != null &&
                 !request.Update.Message.LeftChatMember.IsBot)
            {
                int telegramUserId = refRepository.GetFromId(request.Update.Message.LeftChatMember.Id) ?? -1;

                RemoveReferalCommand removeReferalCommand = new RemoveReferalCommand(request.Update.Message.LeftChatMember.Id);
                mediator.Send(removeReferalCommand).Wait();

                if(telegramUserId != -1)
                {
                    UpdateReferalCountInGoogleDriveCommand updateReferalsCommand = new UpdateReferalCountInGoogleDriveCommand(telegramUserId);
                    mediator.Send(updateReferalsCommand).Wait();
                }

                return true;
            }
            else if (
              request.Update.Message.NewChatMembers != null &&
              request.Update.Message.NewChatMembers.Length > 0)
            {
                var newMembers = request.Update.Message.NewChatMembers;
                foreach (var member in newMembers)
                {
                    var user = userRepository.GetUser(member.Id);
                    if (user != null)
                    {
                        dialogService.SendReplyToUser(request.Update, user);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
