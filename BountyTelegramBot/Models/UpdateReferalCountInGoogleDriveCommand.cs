﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class UpdateReferalCountInGoogleDriveCommand: IRequest<bool>
    {
        public int FromId { get; private set; }

        public UpdateReferalCountInGoogleDriveCommand(int fromId)
        {
            FromId = fromId;
        }
    }
}
