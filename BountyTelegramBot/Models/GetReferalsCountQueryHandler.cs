﻿using BountyTelegramBot.DAL;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class GetReferalsCountQueryHandler: AsyncRequestHandler<GetReferalsCountQuery, int>
    {
        IReferalUserRepository refUserRepository;

        public GetReferalsCountQueryHandler(IReferalUserRepository userRepository)
        {
            this.refUserRepository = userRepository;
        }

        protected override async Task<int> HandleCore(GetReferalsCountQuery request)
        {
            return refUserRepository.GetAllUserReferals(request.TelegramUserId).Count();
        }
    }
}
