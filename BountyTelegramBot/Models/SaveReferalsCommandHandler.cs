﻿using BountyTelegramBot.DAL;
using BountyTelegramBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Models
{
    public class SaveReferalsCommandHandler: AsyncRequestHandler<SaveReferalsCommand, bool>
    {
        IReferalUserRepository referalRepository;
        IGoogleDriveService googleDriveService;

        public SaveReferalsCommandHandler(IReferalUserRepository referalRepository, IGoogleDriveService googleDriveService)
        {
            this.referalRepository = referalRepository;
            this.googleDriveService = googleDriveService;
        }

        protected override async Task<bool> HandleCore(SaveReferalsCommand request)
        {
            foreach (var referal in request.Referals)
            {
                var refs = referalRepository.GetAllUserReferals(request.FromId);
                if (refs.FirstOrDefault(r => r.ReferalUserId == referal.ReferalUserId) == null)
                {
                    referal.FromId = request.FromId;
                    referalRepository.AddReferal(referal);

                    await googleDriveService.ExportUserInfo(request.FromId);
                }
            }
            return true;
        }
    }
}
