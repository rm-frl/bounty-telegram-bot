﻿using BountyTelegramBot.Dialogs;
using BountyTelegramBot.Interfaces;
using BountyTelegramBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BountyTelegramBot.States
{
    public class ShowMenuMyProfileState: BaseState
    {
        TelegramUserInfo UserInfo { get; set; }
        bool IsFollowUser { get; }

        public ShowMenuMyProfileState(BaseDialog Dialog) : base(Dialog)
        {
            GetAllUserInfoQuery query = new GetAllUserInfoQuery(Dialog.TelegramUserId);
            UserInfo = Dialog.Mediator.Send(query).Result;
            if (UserInfo?.TwitterProfileUrl != null)
                IsFollowUser = Dialog.Mediator.Send(new CheckFollowersInTwitterQuery(UserInfo.TwitterProfileUrl)).Result;
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitMenuMyProfileState(Dialog));
        }

        public ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                 new KeyboardButton[]{
                    new KeyboardButton(MyProfileDialog.EDIT_PROFILE_LABEL),
                },
                  new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = CreateProfileInfo() } },
                ParseMode = Telegram.Bot.Types.Enums.ParseMode.Html,
                ReplyMarkup = keyboard
            };
            return reply;
        }

        private string CreateProfileInfo()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("<b>YOUR PROFILE</b>");
            strBuilder.Append("\n");
            strBuilder.AppendFormat("{0} Email : {1}\n", 
                (UserInfo?.Email != null ? "✔️" : "❌"),
                (UserInfo?.Email != null ? UserInfo.Email : "Not filled")
                );
            strBuilder.AppendFormat("{0} Twitter Handle : {1}\n",
                (UserInfo?.TwitterProfileUrl != null ? "✔️" : "❌"),
                (UserInfo?.TwitterProfileUrl != null ? UserInfo.TwitterProfileUrl : "Not filled")
                );
            strBuilder.AppendFormat("{0} Facebook Photo : {1}\n",
                (UserInfo?.FacebookProfile != null ? "✔️" : "❌"),
                (UserInfo?.FacebookProfile != null ? "Attached" : "Not Attached")
                );
            strBuilder.AppendFormat("{0} Instagram Photo : {1}\n",
                (UserInfo?.InstagramProfile != null ? "✔️" : "❌"),
                (UserInfo?.InstagramProfile != null ? "Attached" : "Not Attached")
                );
            strBuilder.AppendFormat("{0} Ethereum Address : {1}\n",
                (UserInfo?.EthereumAddress != null ? "✔️" : "❌"),
                (UserInfo?.EthereumAddress != null ? UserInfo.EthereumAddress : "Not filled")
                );
            strBuilder.AppendFormat("{0} Following @ServisHeroCoin : {1}\n",
                (IsFollowUser == true ? "✔️" : "❌"),
                (IsFollowUser == true ? "Yes" : "No")
                );
            return strBuilder.ToString();
        }
    }
}
