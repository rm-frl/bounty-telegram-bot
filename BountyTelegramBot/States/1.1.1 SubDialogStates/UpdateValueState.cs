﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BountyTelegramBot.Dialogs;
using BountyTelegramBot.Models;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BountyTelegramBot.States
{
    public class UpdateValueState : BaseState
    {
        public UpdateValueState(BaseDialog dialog): base(dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitValueState(Dialog));
        }

        public override ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = false;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Dialog.InitTextMessage } },
                ReplyMarkup = keyboard
            };
            
            return reply;
        }
    }
}
