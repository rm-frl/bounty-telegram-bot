﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BountyTelegramBot.Dialogs;
using BountyTelegramBot.Models;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BountyTelegramBot.States
{
    public class ShowMenuState : BaseState
    {
        public ShowMenuState(BaseDialog Dialog): base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
        }

        public override ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(ENTER_NEW),
                },
                new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Dialog.InitTextMessage } },
                ReplyMarkup = keyboard
            };
            return reply;
        }

        public const string ENTER_NEW = "📝 Enter New Value";

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == ENTER_NEW)
            {
                Dialog.ChangeState(new UpdateValueState(Dialog));
            }
        }
    }
}
