﻿using BountyTelegramBot.Dialogs;
using BountyTelegramBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BountyTelegramBot.States
{
    public class WaitValueState: BaseState
    {
        public WaitValueState(BaseDialog Dialog): base(Dialog) { }

        public override void HandleMessage(Update update)
        {
            if (Dialog.IsValidMessage(update))
            {
                Dialog.SaveSuccessMessage(update);
                Dialog.ChangeState(new SuccessValueState(Dialog));
            }
            else
            {
                Dialog.ChangeState(new ErrorValueState(Dialog));
            }
        }
    }
}
