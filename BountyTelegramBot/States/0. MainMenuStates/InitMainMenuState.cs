﻿using BountyTelegramBot.Dialogs;
using BountyTelegramBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.Enums;

namespace BountyTelegramBot.States
{
    public class InitMainMenuState: BaseState
    {
        public InitMainMenuState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void HandleMessage(Update update)
        {
            CheckMemberInGroupQuery query = new CheckMemberInGroupQuery(Dialog.TelegramUserId);
            string groupName = Dialog.Mediator.Send(query).Result; // return groupName if true, else null

            if (groupName != null)
            {
                Dialog.ChangeState(new ShowMainMenuState(Dialog));
            }
            else
            {
                Dialog.Notify(this, GetReply());
            }
        }

        public ReplyTextMessage GetReply()
        {
            string text = "Hi! Welcome to ServisHero Coin Bounty and Referral Program bot! Start winning tokens by joining the @servisherocoin / https://t.me/servisherocoin Telegram group 😁👍";
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                DisableWebPageView = false
            };
            return reply;
        }

        public override void OnExit()
        {
            string text = "Thanks for joining the Telegram Group 😁👍, press/click on ‘Create my Hero profile’ to key in your\n"+
                            "🔹️Email address,\n"+
                            "🔸️ETH address,\n"+
                            "🔹️Twitter handle,\n"+
                            "🔸️Telegram handle\n"+
                            "and to verify that you have done the following:\n"+
                            "🔹️Submit a Facebook picture showing you have followed ServisHero Coin's Facebook page a\n"+
                            "🔸️Submit an Instagram picture showing you have followed ServisHero Coin's Instagram page 😎👍\n\n"+
                            "You will be awarded with X amount of ServisHero Coins upon submission of these information and completing the actions!";

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
            };

            Dialog.Notify(this, reply);
        }
    }
}
