﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot
{
    public class TelegramConfiguration
    {
        public string BotToken { get; set; }
        public string TargetGroupName { get; set; }
    }
}
