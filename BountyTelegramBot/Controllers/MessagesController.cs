﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BountyTelegramBot.Interfaces;
using BountyTelegramBot.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace BountyTelegramBot.Controllers
{
    public class MessagesController : Controller
    {
        IMediator mediator;
        IClientService botService;

        public MessagesController(IMediator mediator, IClientService botService)
        {
            this.mediator = mediator;
            this.botService = botService;
        }

        [HttpGet]
        public IEnumerable<string> Index()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        public async Task<IActionResult> SetWebHook(string url)
        {
            botService.Client.SetWebhookAsync(url + "/messages/apply").Wait();
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Apply([FromBody]Update update)
        {
            if (update.Message != null)
            {
                try
                {
                    ParseMessageCommand command = new ParseMessageCommand() { Update = update };
                    mediator.Send(command).Wait();
                }
                catch(Exception e)
                {
                    return NotFound();
                }
            }
            return Ok();
        }
    }
}