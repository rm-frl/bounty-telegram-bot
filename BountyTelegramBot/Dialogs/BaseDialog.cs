﻿using BountyTelegramBot.Models;
using BountyTelegramBot.Notifications;
using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BountyTelegramBot.Dialogs
{
    public abstract class BaseDialog
    {
        public BaseDialog(IMediator mediator, int telegramUserId)
        {
            Mediator = mediator;
            TelegramUserId = telegramUserId;
        }

        public IMediator Mediator { get; protected set; }
        public int TelegramUserId { get; protected set; }
        public string Name { get; protected set; }

        public const string RETURN_BACK_LABEL = "🔙 Return To Previous Menu";

        public virtual string InitTextMessage => "Enter please!";
        public virtual string SuccessTextMessage => "👍 Good! We are saved your message!";
        public virtual string ErrorTextMessage => "😞 Sorry! You are filling a wrong type\n* Enter new message*";
        public virtual string UpdateTextMessage => "⌨️ Please! Enter new value";

        private IState currentState;
        public IState CurrentState
        {
            get
            {
                return currentState;
            }
            protected set
            {
                currentState = value;
                SaveStateNotification notification = new SaveStateNotification(TelegramUserId, currentState?.GetType().AssemblyQualifiedName, this.GetType().AssemblyQualifiedName);
                Mediator.Publish(notification);
            }
        }

        public virtual void ChangeState(IState newState)
        {
            CurrentState?.OnExit();
            CurrentState = newState;
            CurrentState?.OnEntry();
        }

        public virtual void Notify(object obj, ReplyTextMessage reply)
        {
            SendReplyTextCommand sendReplyCommand = new SendReplyTextCommand(TelegramUserId, reply);
            Mediator.Send(sendReplyCommand);
        }

        public virtual void HandleMessage(Update update)
        {
            CurrentState?.HandleMessage(update);
        }

        public virtual bool IsValidMessage(Update update)
        {
            return true;
        }

        public virtual void SaveSuccessMessage(Update successMessage)
        {
            var saveMessageNotification = new SaveSuccessMessageNotification(Name, successMessage);
            Mediator.Publish(saveMessageNotification);
        }

    }
}
