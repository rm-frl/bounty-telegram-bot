﻿using BountyTelegramBot.Models;
using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BountyTelegramBot.Dialogs
{
    public class EmailDialog : BaseDialog
    {
        public EmailDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new UpdateValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage => "Please enter your Email address. 😎👍";

        public override bool IsValidMessage(Update update)
        {
            if (update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage
                && update.Message.Text.Contains("@"))
                return true;
            else
                return false;
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
