﻿using BountyTelegramBot.Models;
using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BountyTelegramBot.Dialogs
{
    public class EthereumDialog : BaseDialog
    {
        public EthereumDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new UpdateValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage =>
            "Please key in your Ethereum Address. Please ensure that this is an Ethereum Address which you have absolute control over. 😎👍" +
        "\n\n*NOTE: DO NOT KEY IN ANY EXCHANGE ADDRESS.* ServisHero Coin is not responsible for any loss of bounty tokens due to the inputting of exchange address";


        public override bool IsValidMessage(Update update)
        {
            if (update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage
                && update.Message.Text.StartsWith("0x") && update.Message.Text.Length == 42)
                return true;
            else
                return false;
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
