﻿using BountyTelegramBot.Models;
using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BountyTelegramBot.Dialogs
{
    public class InstagramDialog : BaseDialog
    {
        public InstagramDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new UpdateValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage => "Please send us a screenshot that shows you have followed @servisherocoin on Instagram. 😎👍";
        public override string SuccessTextMessage => "👍 Good! We have saved your screenshot!";
        public override string ErrorTextMessage => "😞 Sorry! We need a screenshot that shows you have followed @servisherocoin on Instagram";
        public override string UpdateTextMessage => "⌨️ Please! Attach screenshot and send me";

        public override bool IsValidMessage(Update update)
        {
            return update.Message.Type == Telegram.Bot.Types.Enums.MessageType.PhotoMessage;
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
