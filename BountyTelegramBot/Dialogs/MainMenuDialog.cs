﻿using BountyTelegramBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Dialogs
{
    public class MainMenuDialog: BaseDialog
    {
        public const string CREATE_PROFILE_LABEL = "👤 Create Your Hero Profile";
        public const string SHOW_DETAILS_LABEL = "📖 Show Details";
        public const string OTHER_LABEL = "📎 Other";

        public MainMenuDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, telegramUserId)
        {
            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    ChangeState((Activator.CreateInstance(stateType, this) as BaseState));
                    return;
                }
            }
            else
            {
                ChangeState(new InitMainMenuState(this));
            }
        }
    }
}
