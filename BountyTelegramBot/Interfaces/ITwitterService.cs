﻿using CoreTweet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Interfaces
{
    public interface ITwitterService
    {
        string TargetGroupName { get; }
        OAuth2Token Token { get; }
        bool IsFollowUser(string screenName, string targetScreeName);
    }
}
