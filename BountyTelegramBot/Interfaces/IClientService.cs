﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;

namespace BountyTelegramBot.Interfaces
{
    public interface IClientService
    {
        TelegramBotClient Client { get; }
    }
}
