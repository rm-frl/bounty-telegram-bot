﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Interfaces
{
    public interface IGoogleDriveService
    {
        Task ExportUserInfo(int telegramUserId);
        Task ExportImageToGoogleDrive(int telegramUserId, string columnName, Stream streamFile);
    }
}
