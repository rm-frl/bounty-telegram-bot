﻿using BountyTelegramBot.Interfaces;
using CoreTweet;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Services
{
    public class TwitterService: ITwitterService
    {
        public string TargetGroupName { get; }

        public OAuth2Token Token { get; private set; }

        public TwitterService(IOptions<TwitterConfiguration> config)
        {
            TargetGroupName = config.Value.TargetGroupName;
            string consumerKey = config.Value.ConsumerKey;
            string consumerSecret = config.Value.ConsumerSecret;

            Token = OAuth2.GetTokenAsync(consumerKey, consumerSecret).Result;
        }

        public bool IsFollowUser(string screenName, string targetScreenName)
        {
            if (screenName.StartsWith('@'))
            {
                screenName = screenName.Substring(1);
            }
            try
            {
                var listFollowers = Token.Followers.ListAsync(targetScreenName).Result;
                return listFollowers.FirstOrDefault(x => x.ScreenName.ToLower() == screenName.ToLower()) != null ? true : false;
            }
            catch
            {
                return false;
            }
        }
    }
}
