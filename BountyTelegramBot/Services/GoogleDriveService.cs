﻿using BountyTelegramBot.DAL;
using BountyTelegramBot.Interfaces;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BountyTelegramBot.Services
{
    public class GoogleDriveService : IGoogleDriveService
    {
        IUserRepository userRepository;
        IUserInfoRepository userInfoRepository;
        IReferalUserRepository refRepository;
        GoogleCredential credential;
        ITwitterService twitterService;

        public GoogleDriveService(
            IUserRepository userRepository,
            IUserInfoRepository userInfoRepository, 
            IReferalUserRepository refRepository, 
            ITwitterService twitterService)
        {
            this.userRepository = userRepository;
            this.refRepository = refRepository;
            this.twitterService = twitterService;
            this.userInfoRepository = userInfoRepository;

            using (var stream = new FileStream("KodeSpeakBountyBot-27a2faa0a2b5.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(
                    SheetsService.Scope.Spreadsheets, 
                    DriveService.Scope.DriveFile, 
                    DriveService.Scope.DriveMetadata, 
                    DriveService.Scope.Drive);
            }
        }

        /// <summary>
        /// <param name="columnName">columnName - Get from SaveSuccessMessageNotificationHandler</param>
        /// <returns></returns>
        public async Task ExportImageToGoogleDrive(int telegramUserId, string columnName, Stream streamFile)
        {
            var user = userRepository.GetUser(telegramUserId);

            var service = new DriveService(new BaseClientService.Initializer
            {
                ApplicationName = "BountyBot",
                HttpClientInitializer = credential
            });

            string fileName = "";

            if (user != null)
            {
                if (columnName.Contains("Facebook"))
                    columnName = "facebook";
                else if (columnName.Contains("Instagram"))
                    columnName = "instagram";

                fileName = String.Format("{0}-{1}-{2}.jpg", user.Id, user.UserName, columnName);

                try {

                    string queryGetFolder = "sharedWithMe and mimeType = 'application/vnd.google-apps.folder'";
                    var folder = GetFirstFileOrFolder(queryGetFolder);

                    if (folder != null)
                    {
                        string queryGetFile = String.Format("'{0}' in parents and name contains '{1}' and name contains '{2}'",
                            folder.Id, user.UserName, columnName);
                        var file = GetFirstFileOrFolder(queryGetFile);

                        var image = new Google.Apis.Drive.v3.Data.File()
                        {
                            Name = fileName,
                            CreatedTime = DateTime.Now,
                            Parents = new List<string> { folder.Id },

                        };

                        if (file != null)
                        {
                            service.Files.Delete(file.Id).Execute();
                        }

                        service.Files.Create(image, streamFile, "image/jpg").Upload();
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        public async Task ExportUserInfo(int telegramUserId)
        {
            var user = userRepository.GetUser(telegramUserId);
            var userInfo = userInfoRepository.GetInfo(telegramUserId);
            if(user == null || userInfo == null)
                return;

            //referals
            var listReferals = refRepository.GetAllUserReferals(telegramUserId);
            int countReferals = 0;
            if (listReferals != null)
                countReferals = listReferals.Count();
            //follow twitter
            bool userIsFollow = false;
            if(userInfo != null && !String.IsNullOrEmpty(userInfo.TwitterProfileUrl))
            {
                userIsFollow = twitterService.IsFollowUser(userInfo.TwitterProfileUrl, twitterService.TargetGroupName);
            }

            var service = new SheetsService(new BaseClientService.Initializer
            {
                ApplicationName = "BountyBot",
                HttpClientInitializer = credential
            });

            var spreadSheetId = "1VBUtx0jQ_DZfSaWEJ3tJ6W2UcGEQ_Nyy7JweoQi1JtE";

            var range = "Sheet1";
            string[][] newValues = new string[][]
            {
                new string[]{
                    DateTime.UtcNow.ToString(),
                    user.Id.ToString(),
                    user.UserName ?? "username not defined",
                    (user.FirstName ?? "") + " " + (user.LastName ?? ""),
                    userInfo.Email ?? "",
                    userInfo.TwitterProfileUrl ?? "",
                    userIsFollow == true ? "Yes" : "No",
                    userInfo.InstagramProfile ?? "",
                    userInfo.FacebookProfile ?? "",
                    countReferals.ToString(),
                    userInfo.EthereumAddress ?? "",
                }
            };
            ValueRange body = new ValueRange();
            body.Range = range;
            body.Values = newValues;

            var requestUsersIdColumn = service.Spreadsheets.Values.Get(spreadSheetId, range + "!B1:B10000");
            var usersIdColumn = requestUsersIdColumn.Execute().Values;

            int positionUserIdInSheet = -1;
            if (usersIdColumn != null)
            {
                positionUserIdInSheet = SearchPositionUserIdnSheets(user.Id, usersIdColumn);
            }
            if (positionUserIdInSheet != -1)
            {
                var listCellData = new List<CellData>();

                for (int i = 0; i < newValues[0].Length; i++)
                {
                    listCellData.Add(new CellData()
                    {
                        UserEnteredValue = new ExtendedValue()
                        {
                            StringValue = newValues[0][i]
                        }
                    });
                }

                var listRequests = new List<Request>()
                {
                    new Request()
                    {
                        UpdateCells = new UpdateCellsRequest()
                        {
                            Rows = new List<RowData>()
                            {
                                new RowData(){ Values = listCellData  }
                            },
                            Fields = "*",
                            Start = new GridCoordinate()
                            {
                                 ColumnIndex = 0,
                                 RowIndex = positionUserIdInSheet,
                            }
                        }
                    }
                };

                BatchUpdateSpreadsheetRequest r = new BatchUpdateSpreadsheetRequest()
                {
                    Requests = listRequests
                };

                var update = service.Spreadsheets.BatchUpdate(r, spreadSheetId);
                var res = update.Execute();
            }
            else
            {
                var append = service.Spreadsheets.Values.Append(body, spreadSheetId, range);
                append.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;
                append.Execute();
            }
        }

        private Google.Apis.Drive.v3.Data.File GetFirstFileOrFolder(string query)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                ApplicationName = "BountyBot",
                HttpClientInitializer = credential
            });

            var request = service.Files.List();
            request.Q = query;
            var files = request.Execute().Files;
            if (files != null && files.Count > 0)
                return files[0];
            else
                return null;
        }

        private int SearchPositionUserIdnSheets(int telegramUserId, IList<IList<object>> column)
        {
            int position = -1;
            column = column.ToArray();
            for (int i = 0; i < column.Count; i++)
            {
                if (column[i][0].ToString() == telegramUserId.ToString())
                    position = i;
            }
            return position;
        }
    }
}
