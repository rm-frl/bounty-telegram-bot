﻿using BountyTelegramBot.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;

namespace BountyTelegramBot.Services
{
    public class ClientService: IClientService
    {
        private readonly TelegramConfiguration _config;

        public ClientService(IOptions<TelegramConfiguration> config)
        {
            _config = config.Value;
            Client = new TelegramBotClient(_config.BotToken);
        }

        public TelegramBotClient Client { get; }
    }
}
