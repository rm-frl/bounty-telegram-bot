﻿using BountyTelegramBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BountyTelegramBot.Notifications
{
    public class CreateUserNotification: INotification
    {
        public TelegramUser User { get; private set; }

        public CreateUserNotification(TelegramUser user)
        {
            User = user;
        }
    }
}
